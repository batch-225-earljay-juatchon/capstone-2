const User = require("../models/user");

const bcrypt = require("bcrypt"); 
const auth = require("../auth");
const user = require("../models/user");


module.exports.userRegister = (reqBody) => {


	let newUser = new User({

		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10) 
	})


	return newUser.save().then((user, error) => {
		if (error) {

			return false;

		} else {

			return user
		};
	});
}; 


module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return {
				message: "Not found in our database"
			}

		} else {

			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);


			if (isPasswordCorrect) {

				return {access : auth.createAccessToken(result)}

			} else {

				// if password doesn't match
				return {
					message: "password was incorrect"
				}
			};

		};

	});
};


//  User Details
module.exports.userDetails = async (userId) => {
	const result = await User.findById(userId);

	return result
};
