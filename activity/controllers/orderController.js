const Order = require("../models/Order");
const Product = require('../models/product');


// Placing an Order directly  Users only
module.exports.checkoutProduct = async (reqBody, userId, data) => {

  try {
   
     if (data.isAdmin == false) {
      
      let totalAmount = await TotalAmount(reqBody);
      
      const Ordered = new Order({

            userId: userId,
            products: reqBody.products,
            totalAmount: totalAmount
      });
      
      await Ordered.save();

      return {
            message: 'Order Created Successfully!'
      };

    } else {

      return {
            message: "Admin is not allowed!"
      }
    }
  } catch (err) {

      return err;

  };
};

// Getting the total amount
const TotalAmount = async (data) => {

     if (data.products.length === 0) {

         return 0;
  };

  let amount = 0;

      for (const product of data.products) {

        try {
            const foundProduct = await Product.findById(product.productId);
             amount += foundProduct.price * product.quantity;
    }   catch (err) {

      return err;

    };
  };
  
  return amount;
};