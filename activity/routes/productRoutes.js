const express = require("express");
const router = express.Router();
const auth = require("../auth")

const productController = require("../controllers/productController");

//  Router for creating a Product
router.post("/addProduct", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(result => res.send(result));
})

//  Router for Retrive all product
router.get("/retrieveAllProducts", auth.verify, (req, res) => {
 
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.retrieveAllProducts(data).then(result => res.send(result));

})


//  Router for Retrieve all Active product

router.get("/retrieveAllActiveProducts", auth.verify, (req, res) => {

	productController.retrieveAllActiveProducts().then(result => res.send(result));


})

//  Router for Retrieve Single product
router.get("/retrieveSingleProduct/:id", auth.verify, (req, res) => {

	const productId = req.params.id

	productController.retrieveSingleProduct(productId).then(result => res.send(result));
})

//  Router for Update product
router.put("/updateProduct/:id", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin

	}
	productController.updateProduct(req.params, req.body, data).then(result => res.send(result));


})

//  Router for Archive product
router.put("/archiveProduct/:id", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin

	}

	productController.archiveProduct(req.params, data).then(result => res.send(result));

})

//  Router for Activate product
router.put("/activateProduct/:id", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin

	}

	productController.activateProduct(req.params, data).then(result => res.send(result));

})








module.exports = router;