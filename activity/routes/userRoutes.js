const express = require("express");
const router = express.Router();
const auth = require("../auth");
//  Import
const userController = require("../controllers/userController");

//  Route for registration
router.post("/userRegistration", (req, res) => {
    userController.userRegister(req.body).then(result => res.send(result));
});

//  Route for user authentication

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(result => res.send(result));
})

//  Route for user Details
router.get("/userDetails/:id", auth.verify, (req, res) => {
    const userId = req.params.id
    userController.userDetails(userId).then(result => res.send(result));
})


module.exports = router;