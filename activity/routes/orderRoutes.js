const express = require("express");
const router = express.Router();
const auth = require("../auth")


const orderController = require("../controllers/orderController");


//  Router for Checkout order
// create order directly
router.post('/checkOut', auth.verify, (req, res) => {
    const data = {
      order: req.body,
      isAdmin: auth.decode(req.headers.authorization).isAdmin
    };
    const userId = auth.decode(req.headers.authorization).id;
    orderController.checkoutProduct(req.body, userId, data).then((result) => {
      res.send(result)
    });
  });






module.exports = router;